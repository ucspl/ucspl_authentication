package com.ucspl.authService.exception;


public class UserNotFoundException extends Exception {

    public UserNotFoundException(String message) {
        super(message);
    }
}
