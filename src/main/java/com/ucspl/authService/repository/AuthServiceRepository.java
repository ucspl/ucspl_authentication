package com.ucspl.authService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.authService.model.AuthServiceModel;

/*
* This class is implementing the JpaRepository interface for AuthServiceModel- user_master table.
* Annotate this class with @Repository annotation
* */
@Repository
public interface AuthServiceRepository extends JpaRepository<AuthServiceModel, Long> {

	/*
	 * Apart from the standard CRUD methods already available in JPA Repository,
	 * based on our requirements, we might need to create few query methods for
	 * getting specific data from the database.
	 */

	// This method will validate a user from database by username and password.
	AuthServiceModel findByUserNameAndPassword(String userName, String userPassword);

	// This method will validate a user from database by username.
	AuthServiceModel findByUserName(String userName);
}
