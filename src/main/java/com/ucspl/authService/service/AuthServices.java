package com.ucspl.authService.service;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ucspl.authService.AuthServiceApplication;
import com.ucspl.authService.exception.UserNotFoundException;
import com.ucspl.authService.model.AuthServiceModel;
import com.ucspl.authService.repository.AuthServiceRepository;

import ch.qos.logback.classic.Logger;

@Service
public class AuthServices {

	static final Logger logger = (Logger)LoggerFactory.getLogger(AuthServices.class);
	
	@Autowired
	AuthServiceRepository authServiceRepository;

	public AuthServiceModel findByUserIdAndPassword(String userId, String password) throws UserNotFoundException {

		final AuthServiceModel user = authServiceRepository.findByUserNameAndPassword(userId, password);
		if (null != user) {
			return user;
		} else {
			logger.info("User not found");
			throw new UserNotFoundException("user not found");
		}

	}

}
