package com.ucspl.authService.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.ucspl.authService.model.AuthServiceModel;
import com.ucspl.authService.repository.AuthServiceRepository;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	static final Logger log = LoggerFactory.getLogger(JwtRequestFilter.class);
	org.slf4j.Marker marker;

	HashSet<String> set = new HashSet();

	private Set<String> skipUrls = new HashSet<>(Arrays.asList("/login"));

	private static final String[] excluded_urls = { "/login", "/auth" };

	private AntPathMatcher pathMatcher = new AntPathMatcher();

	@Autowired
	AuthServiceRepository authServiceRepository;

	// avoid filtering of the given request.
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) {

		return skipUrls.stream().anyMatch(p -> pathMatcher.match(p, request.getServletPath()));
	}

	/*
	 * this method will get call for every url except "/login" and it will check
	 * whether token is present in url or not. If token is present then it will
	 * allow to access the url otherwise it will throw exception as authentication
	 * token is missing.
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException, UsernameNotFoundException {

		log.info(marker, "path is {}", request.getServletPath());

		final HttpServletRequest req = (HttpServletRequest) request;
		String authHeader = null;
		authHeader = ((HttpServletRequest) req).getHeader("Authorization");

		chain.doFilter(request, response);

	}

	// for getting claim from token - used for separating username from token
	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}
	

	
	
	
	
	// for retrieveing any information from token we will need the secret key
	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
	}

	// check if username is present in database or not. If username is not present
	// in database then throw exception otherwise return userdetails.
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		AuthServiceModel user = authServiceRepository.findByUserName(username);
		if (user == null) {

			log.error(marker, "User not found with username {}", username);
			return new org.springframework.security.core.userdetails.User(null, null, new ArrayList<>());

		}
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
				new ArrayList<>());
	}

}