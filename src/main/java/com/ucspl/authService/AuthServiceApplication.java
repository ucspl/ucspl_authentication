package com.ucspl.authService;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ch.qos.logback.classic.Logger;

@SpringBootApplication
public class AuthServiceApplication {

	static final Logger logger = (Logger) LoggerFactory.getLogger(AuthServiceApplication.class);
	static org.slf4j.Marker marker;

	public static void main(String[] args) {
		SpringApplication.run(AuthServiceApplication.class, args);

		String name = "Authentication";
		logger.info("Authentication service");
		logger.info("{} application start ", name);
	}

}
