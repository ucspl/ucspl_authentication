package com.ucspl.authService.controller;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.authService.exception.UserNotFoundException;
import com.ucspl.authService.model.AuthServiceModel;
import com.ucspl.authService.service.AuthServices;

import ch.qos.logback.classic.Logger;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@CrossOrigin(origins = "*")
public class AuthServiceController {

	static final Logger logger = (Logger) LoggerFactory.getLogger(AuthServiceController.class);

	HashMap<String, String> map = new HashMap<>();

	@Autowired
	AuthServices authicationService;

	@RequestMapping({ "/hello" })
	public String firstPage() {
		return "Hello World";
	}

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody AuthServiceModel user) {

		AuthServiceModel userDetails = new AuthServiceModel();
		String fullName = null;
		System.out.println(user.getUserName() + "" + user.getPassword());
		try {
			final String token = getToken(user.getUserName(), user.getPassword());

			if (token != null && token != "") {
				userDetails = authicationService.findByUserIdAndPassword(user.getUserName(), user.getPassword());
				fullName = userDetails.getFirstName() + " " + userDetails.getMiddleName() + " "
						+ userDetails.getLastName();
			}
			map.clear();
			map.put("token", token);
			map.put("name", fullName);
			map.put("message", "User logged in successfully");
			logger.info("User logged in successfully");

		} catch (Exception e) {

			e.printStackTrace();
			map.clear();
			map.put("token", null);
			map.put("message", e.getMessage());
			logger.info("User is not authorized");
			return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<>(map, HttpStatus.OK);

	}

	@PostMapping("/auth")
	public ResponseEntity<?> isAuthenticate(ServletRequest request) throws ServletException {

		final HttpServletRequest req = (HttpServletRequest) request;

		final String authHeader = req.getHeader("Authorization");

		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			logger.info("Authorization token is missing");
			// throw new ServletException("Authorization token is missing");

			map.clear();
			map.put("authHeader", null);
			map.put("message", "Authorization token is missing");
			logger.info("User is not authorized");
			return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);

		}
		final String token = authHeader.substring(7);
		final Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
		req.setAttribute("claims", claims);
		final HashMap<String, Boolean> map = new HashMap<>();
		map.clear();
		map.put("isAuthenticated", true);
		return new ResponseEntity<>(map, HttpStatus.OK);
//	    	
	}

	// Generate JWT token
	public String getToken(String username, String password) throws UsernameNotFoundException {
		AuthServiceModel user = new AuthServiceModel();

		try {
			user = authicationService.findByUserIdAndPassword(username, password);
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (user.getUserName() != null) {
			final String jwttoken = Jwts.builder().setSubject(username)
					.setIssuedAt(new Date(System.currentTimeMillis())).signWith(SignatureAlgorithm.HS256, "secretkey")
					.compact();
			logger.info("Token generated successfully");
			return jwttoken;
		} else
			throw new UsernameNotFoundException("User not found with username: " + username);

	}

}
